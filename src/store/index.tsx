import { applyMiddleware, createStore } from "redux";

import appReducer from "./reducers";
import thunk from "redux-thunk";

export default () => {
    return applyMiddleware(thunk)(createStore)(appReducer);
};
