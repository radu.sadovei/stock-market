import C from "../utils/constants";
import { DispatchData } from "../interfaces/interfaces";

export const changeSymbol = (symbol: string): DispatchData => {
  return {
    type: C.CHANGE_SYMBOL,
    payload: symbol,
  };
};

export const addError = (message: string): DispatchData => ({
  type: C.ADD_ERROR,
  payload: message,
});

export const clearError = (index: number): DispatchData => ({
  type: C.CLEAR_ERROR,
  payload: index,
});

export const setFlag = (flag: boolean): DispatchData => ({
  type: C.SET_FLAG,
  payload: flag,
});

export const fetchingData = (value: string) => (dispatch: any) => {
  fetch(
    `https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=${value}&interval=5min&outputsize=compact&apikey=3LJ3V4ISRGE2B1D9`
  )
    .then((response) => response.json())
    .then((data) => {
      dispatch({
        type: C.CREATE_API_REQUEST,
        payload: Object.entries(data["Monthly Time Series"])
          .map(([date, singleInfo]: any) => {
            return {
              date,
              price: parseFloat(singleInfo["4. close"]),
            };
          })
          .sort((a, b) => (a.date < b.date ? -1 : 1)),
      });
    })

    .catch((error) => {
      dispatch(addError(error.message));

      dispatch({
        type: C.CLEAR_ERROR,
      });
    });
};
