import C from "../utils/constants";
import { combineReducers } from "redux";
import { DispatchData } from '../interfaces/interfaces'

export const symbol = (state = "", action: DispatchData) =>
    action.type === C.CHANGE_SYMBOL ? action.payload : state;

export const data = (state = [], action: any) =>
    action.type === C.CREATE_API_REQUEST ? (state = action.payload) : state;

export const errors = (state = [], action: DispatchData) => {
    switch (action.type) {
        case C.ADD_ERROR:
            return [...state, action.payload];
        case C.CLEAR_ERROR:
            return (state = []);
        default:
            return state;
    }
};

export const flagFilter = (state = false, action: any) => action.type === C.SET_FLAG ? (state = action.payload) : state;

export default combineReducers({
    symbol,
    data,
    errors,
    flagFilter
});
