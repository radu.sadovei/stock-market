import { changeSymbol, fetchingData } from "./store/actions";
import InputInterval from "./components/inputInterval/inputInterval";
import InputSymbol from "./components/inputSymbol/inputSymbol";
import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";


interface AppProps {
    dispatch: Dispatch<any>;
}

function App(props: AppProps) {
    props.dispatch(changeSymbol("AMZN"));
    props.dispatch(fetchingData("AMZN"));

    return (
        <div className="App">
            <InputSymbol />
            <InputInterval />
        </div>
    );
}

export default connect()(App);
