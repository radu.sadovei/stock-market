const constants = {
    CREATE_API_REQUEST: "CREATE_API_REQUEST",
    CHANGE_SYMBOL: "CHANGE_SYMBOL",

    ADD_ERROR: "ADD_ERROR",
    CLEAR_ERROR: "CLEAR_ERROR",
    SET_FLAG: 'SET_FLAG',
}

export default constants