import "./inputSymbol.scss";

import React, { useRef } from "react";
import {
    changeSymbol,
    fetchingData,
    setFlag
} from "../../store/actions";
import { connect } from "react-redux";

interface InputProps {
    symbol: string,
    flagFilter: boolean
}

const InputSymbol = (props: any) => {
    let symbolValue = props.symbol;
    let inputRef: any = useRef(null);
    return (
        <div className="input-symbol-wrapper">
            <input
                className="input-symbol"
                ref={inputRef}
                defaultValue={symbolValue}
                placeholder="Enter Symbol"
            />

            <button
                className="button-symbol"
                onClick={() => {
                    props.setFlag(false);
                    props.changeSymbolValue(inputRef.current.value);
                    props.changeDataAPI(inputRef.current.value);
                }}
            >
                Get Data
			</button>
        </div>
    );
};

const mapStateToProps = (state: InputProps) => ({
    symbol: state.symbol,
    flagFilter: state.flagFilter
});

const mapDispatchToProps = (dispatch: any) => ({
    changeSymbolValue(value: string) {
        dispatch(changeSymbol(value));
    },

    changeDataAPI(value: any) {
        dispatch(fetchingData(value));
    },

    setFlag(flag: boolean) {
        dispatch(setFlag(flag));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(InputSymbol);
