import "./chart.scss";
import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis, Label, ReferenceLine } from 'recharts';
import { ChartProps } from '../../interfaces/interfaces'
import React from "react";

const Chart = (props: ChartProps) => {
    const { data, errors, dataFiltered, flagInterval } = props;

    const AveragePrice = (stockData: any): number => {

        let prices = stockData.map((value: any): number => value.price)
        let sum = prices.reduce((a: number, b: number) => a + b, 0)
        let average = sum / stockData.length

        return average
    }

    let averagePrice = AveragePrice(!flagInterval ? data : dataFiltered)

    return (
        <>
            <LineChart
                width={1200}
                height={300}
                data={!flagInterval ? data : dataFiltered}
            >
                <XAxis dataKey="date" />
                <YAxis dataKey="price" />
                <CartesianGrid stroke="red" strokeDasharray="5 5" />
                <Line type="monotone" dataKey="price" stroke="blue" />
                <Tooltip />
                <ReferenceLine y={averagePrice} stroke="sandybrown" strokeDasharray='5 10'>
                    <Label value={`avg: ${averagePrice.toFixed(2)}`} position='insideLeft' fill='black' fontWeight='bold' />
                </ReferenceLine>
            </LineChart>
            {errors.length
                ? errors.map((message: string, i: number): any => (
                    <div key={i} className="error">
                        <p>{message}</p>
                    </div>
                ))
                : null}
        </>
    );
};

export default Chart;
