import "./inputInterval.scss";

import React, { useState } from "react";
import { addError, clearError, setFlag } from "../../store/actions";

import Chart from "../chart/chart";
import { InputIntervalProps } from "../../interfaces/interfaces";
import { connect } from "react-redux";

const InputInterval = (props: any) => {
  const [startValue, setStartValue] = useState("");
  const [endValue, setEndValue] = useState("");
  const [newValues, setNewValues] = useState(props.data);

  return (
    <div className="interval-chart-wrapper">
      <Chart
        data={props.data}
        errors={props.errors}
        dataFiltered={newValues}
        flagInterval={props.flagFilter}
      />

      <div className="interval-wrapper">
        <select
          className="select-interval"
          onChange={(e: any) => setStartValue(e.target.value)}
        >
          <option value="-1">Start Interval</option>

          {props.data.map((itemStart: any, index: number): any => (
            <option key={index} value={index}>
              {itemStart.date}
            </option>
          ))}
        </select>

        <select
          className="select-interval"
          onChange={(e: any) => setEndValue(e.target.value)}
        >
          <option value="-1">End Interval</option>

          {props.data.map((itemStart: any, index: number) => (
            <option key={index} value={index}>
              {itemStart.date}
            </option>
          ))}
        </select>

        <button
          className="button-symbol"
          onClick={() => {
            if (
              parseInt(startValue) < 0 ||
              parseInt(endValue) < 0 ||
              startValue === null ||
              endValue === null
            ) {
              props.clearError();
              props.addError("Select Interval Value!");
            } else if (parseInt(startValue) < parseInt(endValue)) {
              props.setFlag(true);
              setNewValues(
                props.data.slice(parseInt(startValue), parseInt(endValue))
              );
              props.clearError();
            } else {
              props.clearError();
              props.addError("Wrong Interval!");
            }
          }}
        >
          Update Interval
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state: InputIntervalProps) => ({
  data: state.data,
  errors: state.errors,
  flagFilter: state.flagFilter,
});

const mapDispatchToProps = (dispatch: any) => ({
  addError(index: string) {
    dispatch(addError(index));
  },

  clearError(index: number) {
    dispatch(clearError(index));
  },

  setFlag(flag: boolean) {
    dispatch(setFlag(flag));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(InputInterval);
