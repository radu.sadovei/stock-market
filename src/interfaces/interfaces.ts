export interface DispatchData {
	type: string;
	payload: string | number | boolean;
}

export interface CreateStore {
	thunk: any;
	createStore: any;
	appReducer: any;
}

export interface TimeSeries {
	date: string;
	price: number;
}

export interface ChartProps {
	data: TimeSeries[];
	errors: any;
	dataFiltered: any;
	flagInterval: boolean;
}

export interface InputIntervalProps {
	data: TimeSeries[];
	errors: any;
	flagFilter: boolean | null;
}


